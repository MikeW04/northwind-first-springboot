package com.training.northwind.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShipperTest {

    private static final String testCompanyName = "Test Company";
    private static final String testPhone = "0123456";

    private Shipper shipper;

    @BeforeEach
    public void setup(){
        this.shipper = new Shipper();
    }

    @Test
    public void setCompanyTest(){

        this.shipper.setCompanyName(testCompanyName);
        assertEquals(this.shipper.getCompanyName(), testCompanyName);

    }

    @Test
    public void setPhoneTest(){

        this.shipper.setPhone(testPhone);
        assertEquals(this.shipper.getPhone(), testPhone);

    }
}
